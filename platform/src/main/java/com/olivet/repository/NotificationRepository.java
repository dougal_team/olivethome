package com.olivet.repository;

import com.olivet.entity.Notification;
import com.olivet.entity.OlivetUser;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "notifications", path = "notification")
public interface NotificationRepository extends PagingAndSortingRepository<Notification, Long> {
    List<Notification> getNotificationByOlivetUser(OlivetUser user, Pageable pageable);
    List<Notification> getNotificationByOlivetUserAndIsRead(OlivetUser user, boolean isRead, Pageable pageable);

}

package com.olivet.entity;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Notification {
    @Id
    @GeneratedValue
    @Column(name = "notification_id")
    private long id;

    private String text;

    private boolean isRead;

    private Date time;

    @ManyToOne
    private OlivetUser olivetUser;

    public Notification(){}

    public Notification(String text){
        this.text = text;
        this.isRead = false;
        this.time = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRead() {
        return isRead;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public OlivetUser getUser() {
        return olivetUser;
    }

    public void setUser(OlivetUser user) {
        this.olivetUser = user;
    }
}

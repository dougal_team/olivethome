package com.olivet.repository;

import com.olivet.entity.OlivetUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<OlivetUser, Long> {
    OlivetUser findByEmail(String email);
}

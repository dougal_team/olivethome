package com.olivet.controller;

import com.olivet.GCLoader;
import com.olivet.entity.OlivetUser;
import com.olivet.repository.DeviceStateRepository;
import com.olivet.repository.LocationRepository;
import com.olivet.repository.NotificationRepository;
import com.olivet.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UserProfileController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DeviceStateRepository stateRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    Logger logger = LoggerFactory.getLogger(UserProfileController.class);

    @GetMapping("/profile")
    public ModelAndView profile(){
        OlivetUser currentUser = getOlivetUser();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", currentUser);
        modelAndView.setViewName("profile");

        GCLoader.generateGarbage();

        return modelAndView;
    }

    private OlivetUser getOlivetUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        return userRepository.findByEmail(currentPrincipalName);
    }

    @PostMapping("/profile")
    public ModelAndView saveProfile(@Valid OlivetUser user, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();
        OlivetUser dbUser = userRepository.findByEmail(user.getEmail());

        saveConfigChanges(user, dbUser);

        modelAndView.addObject("successMessage", "Изменения сохранены!");
        modelAndView.addObject("user", user);
        modelAndView.setViewName("profile");

        return modelAndView;
    }

    private void saveConfigChanges(@Valid OlivetUser user, OlivetUser dbUser) {
        dbUser.setUseHub(user.isUseHub());
        dbUser.setUseGreenhouse(user.isUseGreenhouse());
        dbUser.setMinSoilHumidity(user.getMinSoilHumidity());
        dbUser.setGreenhouseRefreshTime(user.getGreenhouseRefreshTime());
        dbUser.setNotifyMinSoil(user.isNotifyMinSoil());
        dbUser.setName(user.getName());
        dbUser.setEmail(user.getEmail());
        dbUser.setUseSmartCamera(user.isUseSmartCamera());
        dbUser.setNotifyMotion(user.isNotifyMotion());

        userRepository.save(dbUser);
    }



}

package com.olivet.controller;

import com.olivet.WebMvcConfig;
import com.olivet.entity.OlivetUser;
import com.olivet.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@RestController
public class FileUploadRestController {

    private WebMvcConfig config;
    private UserRepository userRepository;

    public FileUploadRestController(WebMvcConfig config, UserRepository userRepository) {
        this.config = config;
        this.userRepository = userRepository;
    }

    @GetMapping("/files")
    public String getFile(){
        return "files";
    }

    @PostMapping("/files")
    public ResponseEntity<String> uploadData(@RequestParam MultipartFile files) throws IOException {

        OlivetUser currentUser = getOlivetUser();

        if(currentUser == null){
            throw new RuntimeException("No current user");
        }

        if(files == null || files.isEmpty()){
            throw new RuntimeException("You must select a file for uploading");
        }

        InputStream inputStream = files.getInputStream();
        String originalName = files.getOriginalFilename();
        String name = files.getName();
        String contentType = files.getContentType();
        System.out.println(contentType);
        long size = files.getSize();

        saveUploadedFiles(Arrays.asList(files), currentUser.getId());

        return new ResponseEntity<String>(originalName, HttpStatus.OK);
    }

    private void saveUploadedFiles(List<MultipartFile> files, long id) throws IOException {

        for(MultipartFile file: files){
            if(file.isEmpty()){
                continue;
            }

            byte[] bytes = file.getBytes();
            String directoryPath = config.getUploadFolder() + id;
            String filePath = directoryPath +"/" + file.getOriginalFilename();

            Path path = Paths.get(directoryPath);
            if(!Files.exists(path)){
                Files.createDirectory(path);
            }

            path = Paths.get(filePath);

            Files.write(path, bytes);
        }
    }

    private OlivetUser getOlivetUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        return userRepository.findByEmail(currentPrincipalName);
    }
}

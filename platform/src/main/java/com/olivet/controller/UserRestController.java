package com.olivet.controller;

import com.olivet.dto.UserDto;
import com.olivet.entity.OlivetUser;
import com.olivet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRestController {

    @Autowired
    private UserService userService;

    @RequestMapping("/api/user")
    public UserDto user() {
        System.out.println("getting principal");
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails details = (UserDetails)principal;

        String username = details.getUsername();
        boolean nonExpired = details.isAccountNonExpired();

        OlivetUser olivetUser = userService.findUserByEmail(username);

        UserDto dto = new UserDto(olivetUser.getId(), username, nonExpired, olivetUser.getLocations().get(0).getId());
        dto.setRefreshTime(olivetUser.getGreenhouseRefreshTime());

        return dto;
    }
}

package com.olivet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import java.lang.management.*;
import java.util.*;

@SpringBootApplication
public class PlatformApplication extends SpringBootServletInitializer {

    protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
        return application.sources(PlatformApplication.class);
    }

	public static void main(String[] args) {
        for (String a : args){
            System.out.println("Argument: " + a);
        }
        System.out.println("Current max memory(Xmx): " + Runtime.getRuntime().maxMemory());
        System.out.println("Current memory (Xms): " + Runtime.getRuntime().totalMemory());

        RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
        List<String> aList = bean.getInputArguments();

        for (int i = 0; i < aList.size(); i++) {
            System.out.println( aList.get( i ) );
        }

		SpringApplication.run(PlatformApplication.class, args);
	}
}


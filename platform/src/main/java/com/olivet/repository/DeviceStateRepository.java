package com.olivet.repository;

import com.olivet.entity.DeviceState;
import com.olivet.entity.Location;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Date;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "devicestate", path = "devicestate")
public interface DeviceStateRepository extends PagingAndSortingRepository<DeviceState, Long> {
    List<DeviceState> findAllByLocationAndTimeAfter(Location location, Date startTime);
}

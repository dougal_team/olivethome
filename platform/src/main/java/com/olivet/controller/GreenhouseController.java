package com.olivet.controller;

import com.olivet.GCLoader;
import com.olivet.entity.DeviceState;
import com.olivet.entity.DeviceType;
import com.olivet.entity.Location;
import com.olivet.entity.OlivetUser;
import com.olivet.exception.LocationNotFoundException;
import com.olivet.repository.DeviceStateRepository;
import com.olivet.repository.LocationRepository;
import com.olivet.repository.NotificationRepository;
import com.olivet.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class GreenhouseController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DeviceStateRepository stateRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    Logger logger = LoggerFactory.getLogger(UserProfileController.class);

    @GetMapping("/greenhouse")
    public ModelAndView greenhouse(){

        logger.debug("greenhouse() - called");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        ModelAndView modelAndView = new ModelAndView();
        setMonitoredData(modelAndView, auth.getName());
        modelAndView.setViewName("greenhouse");

        GCLoader.generateGarbage();

        return modelAndView;
    }

    @PostMapping("/greenhouse")
    public ModelAndView saveLocation(@Valid Location location, BindingResult bindingResult){
        System.out.println(location);
        Optional<Location> dbLocation = locationRepository.findById(location.getId());

        if(!dbLocation.isPresent())
            throw new LocationNotFoundException();

        Location updatedLocation = dbLocation.get();
        updatedLocation.setEnableAutoFill(location.isEnableAutoFill());
        updatedLocation.setEnableAutoWatering(location.isEnableAutoWatering());
        updatedLocation.setMinHumidity(location.getMinHumidity());
        updatedLocation.setWateringIntervalInSeconds(location.getWateringIntervalInSeconds());

        locationRepository.save(updatedLocation);


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        ModelAndView modelAndView = new ModelAndView();
        setMonitoredData(modelAndView, auth.getName());
        modelAndView.setViewName("greenhouse");
        return modelAndView;
    }

     /*
        Setting all locations and sensors data for current user
     */
    private void setMonitoredData(ModelAndView model, String name){

        logger.debug("setMonitoredData() - getting olivetUser {0} data", name);

        OlivetUser olivetUser = userRepository.findByEmail(name);
        model.addObject("username", olivetUser.getName());
        List<Location> locations = olivetUser.getLocations();

        if(locations != null && !locations.isEmpty()){

            Location currentLocation = locations.get(0);
            model.addObject("location", currentLocation.getName());

            Calendar c = Calendar.getInstance() ;
            c.add(Calendar.HOUR,-24);
            Date startDate = c.getTime();

            model.addObject("currentLocation", currentLocation);
            Pageable topPagable = new PageRequest(0, 10, Sort.Direction.ASC, "time");
            model.addObject("notifications", notificationRepository.getNotificationByOlivetUser(olivetUser, topPagable));
            model.addObject("notReadNotifications", notificationRepository.getNotificationByOlivetUserAndIsRead(olivetUser, false, topPagable));


            List<DeviceState> states = stateRepository.findAllByLocationAndTimeAfter(currentLocation, startDate);

            DeviceState soilState = states.stream().filter(x->x.getType() == DeviceType.SOIL_HUMIDITY).reduce((first, second) -> second).orElse(null);
            model.addObject("soilState", soilState==null?0:soilState.getValue());

            DeviceState airHumState = states.stream().filter(x->x.getType() == DeviceType.AIR_HUMIDITY).reduce((first, second) -> second).orElse(null);
            model.addObject("airHumidity", airHumState==null?0:airHumState.getValue());

            DeviceState temperatureState = states.stream().filter(x->x.getType() == DeviceType.AIR_TEMPERATURE).reduce((first, second) -> second).orElse(null);
            model.addObject("airTemperature", temperatureState==null?0:temperatureState.getValue());

            DeviceState waterTemp = states.stream().filter(x->x.getType() == DeviceType.WATER_TEMPERATURE).reduce((first, second) -> second).orElse(null);
            model.addObject("waterTemperature", waterTemp==null?0: waterTemp.getValue());


            long skeepNumber = states.stream().filter(x->x.getType() == DeviceType.SOIL_HUMIDITY).count() - 5;
            List<Integer> lastStates;
            if(skeepNumber > 0){
                lastStates = states.stream().filter(x->x.getType() == DeviceType.SOIL_HUMIDITY).skip(skeepNumber).map(x->x.getValue())
                        .limit(5).collect(Collectors.toList());
            }
            else {
                lastStates = states.stream().filter(x->x.getType() == DeviceType.SOIL_HUMIDITY).map(x->x.getValue()).limit(5).collect(Collectors.toList());
            }


            model.addObject("soilStates", lastStates);
        }
    }
}

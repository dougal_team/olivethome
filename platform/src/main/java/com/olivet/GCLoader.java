package com.olivet;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Slf4j
public class GCLoader {
    public static void generateGarbage(){
        log.info("Generating garbage");

        Random random = new Random();

        int n = 600;

        List list = new LinkedList();
        for (int i = 0; i < n; i++) {

            String[] str = new String[2048];

            list.add(str);

            if (i % 200 == 0) {
                //System.out.println("i=" + i);
                try {

                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
            }
        }

        Integer[] str = new Integer[1500];

        for(int j=0;j < 1500;j++){
            str[j] = random.nextInt();
        }

        Arrays.sort(str);

        log.info("Garbage ready for use!");
    }
}

package com.olivet.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class OlivetUser {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    @Email
    private String email;

    @NotEmpty
    @Column(name="password")
    @Length(min=5)
    @JsonIgnore
    private String password;

    @Column(name = "active")
    private int active;

    @Column(name = "use_greenhouse")
    private boolean useGreenhouse;

    @Column(name = "use_hub")
    private boolean useHub;

    @Column(name = "use_smart_camera")
    private boolean useSmartCamera;

    @Column(name = "greenhouse_refresh")
    private int greenhouseRefreshTime;

    @Column(name = "min_soil_humidity")
    private int minSoilHumidity;

    @Column(name = "notify_min_soil")
    private boolean notifyMinSoil;

    @Column(name = "notify_motion")
    private boolean notifyMotion;

    @OneToMany(mappedBy = "olivetUser",cascade = {CascadeType.ALL})
    private List<Location> locations = new ArrayList<>();

    @OneToMany(mappedBy = "olivetUser", cascade = {CascadeType.ALL})
    private List<Notification> notifications = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public OlivetUser(){}

    public OlivetUser(String email, String name){
        this.email = email;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locationsList) {
        this.locations = locationsList;
    }

    @PrePersist
    @PreUpdate
    public void updateLocationsAssociation(){
        for(Location location: this.locations){
            location.setOlivetUser(this);
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public boolean isUseGreenhouse() {
        return useGreenhouse;
    }

    public void setUseGreenhouse(boolean useGreenhouse) {
        this.useGreenhouse = useGreenhouse;
    }

    public boolean isUseHub() {
        return useHub;
    }

    public void setUseHub(boolean useHub) {
        this.useHub = useHub;
    }

    public int getGreenhouseRefreshTime() {
        return greenhouseRefreshTime;
    }

    public void setGreenhouseRefreshTime(int greenhouseRefreshTime) {
        this.greenhouseRefreshTime = greenhouseRefreshTime;
    }

    public int getMinSoilHumidity() {
        return minSoilHumidity;
    }

    public void setMinSoilHumidity(int minSoilHumidity) {
        this.minSoilHumidity = minSoilHumidity;
    }

    public boolean isNotifyMinSoil() {
        return notifyMinSoil;
    }

    public void setNotifyMinSoil(boolean notifyMinSoil) {
        this.notifyMinSoil = notifyMinSoil;
    }

    public boolean isNotifyMotion() {
        return notifyMotion;
    }

    public void setNotifyMotion(boolean notifyMotion) {
        this.notifyMotion = notifyMotion;
    }

    public boolean isUseSmartCamera() {
        return useSmartCamera;
    }

    public void setUseSmartCamera(boolean useSmartCamera) {
        this.useSmartCamera = useSmartCamera;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }
}

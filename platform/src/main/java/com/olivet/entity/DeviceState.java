package com.olivet.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class DeviceState implements Comparable<DeviceState> {

    @Id
    @GeneratedValue
    private long id;

    @Enumerated(EnumType.STRING)
    private DeviceType type;
    private int value;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;
    @ManyToOne
    @JoinColumn(name="location_id")
    private Location location;

    public DeviceState(){}

    public DeviceState(DeviceType type, int value, Date time){
        this.type = type;
        this.value = value;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DeviceType getType() {
        return type;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public int compareTo(DeviceState state) {
        if (getTime() == null || state.getTime() == null) {
            return 0;
        }
        return getTime().compareTo(state.getTime());
    }
}

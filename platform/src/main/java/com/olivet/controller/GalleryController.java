package com.olivet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GalleryController {

    @GetMapping("gallery")
    public ModelAndView cameraPage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("gallery");

        return modelAndView;
    }
}

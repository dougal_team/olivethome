package com.olivet.controller;

import com.olivet.GCLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @GetMapping("/smarthouse")
    public ModelAndView home(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("smarthouse");
        GCLoader.generateGarbage();
        return modelAndView;
    }
}

package com.olivet.controller;

import com.olivet.GCLoader;
import com.olivet.entity.Location;
import com.olivet.entity.LocationType;
import com.olivet.entity.Notification;
import com.olivet.entity.OlivetUser;
import com.olivet.repository.DeviceStateRepository;
import com.olivet.repository.LocationRepository;
import com.olivet.repository.NotificationRepository;
import com.olivet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private DeviceStateRepository deviceStateRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @RequestMapping("/index")
    public String home(){
        GCLoader.generateGarbage();
        return "index";
    }

    @RequestMapping("/login")
    public String login(){
        GCLoader.generateGarbage();
        return "login";
    }

    @RequestMapping(value = "registration", method = RequestMethod.GET)
    public ModelAndView registration(){
        ModelAndView modelAndView = new ModelAndView();

        OlivetUser user = new OlivetUser();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("registration");

        GCLoader.generateGarbage();

        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid OlivetUser user, BindingResult bindingResult){

        ModelAndView modelAndView = new ModelAndView();
        OlivetUser userExists = userService.findUserByEmail(user.getEmail());

        if(userExists != null){
            bindingResult
                    .rejectValue("email", "error.user","Пользователь с таким email уже существует");
        }

        if(bindingResult.hasErrors()){
            modelAndView.addObject("user", user);
            modelAndView.setViewName("registration");
        }
        else {

            saveDefaultParams(user);

            modelAndView.addObject("successMessage", "Пользователь зарегестрирован!");
            modelAndView.addObject("user", new OlivetUser());
            modelAndView.setViewName("registration");
        }

        return modelAndView;
    }



    private void saveDefaultParams(@Valid OlivetUser user) {
        Location location = new Location();
        location.setOlivetUser(user);
        location.setType(LocationType.GREENHOUSE);
        location.setName("Default");

        Notification notification = new Notification("Новый пользователь зарегестрирован");
        notification.setUser(user);

        user.setGreenhouseRefreshTime(10000);
        user.setNotifyMinSoil(false);
        user.setMinSoilHumidity(25);
        user.setUseGreenhouse(true);
        user.setUseHub(false);

        userService.saveUser(user);
        locationRepository.save(location);
        notificationRepository.save(notification);
    }
}

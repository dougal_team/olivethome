package com.olivet.repository;

import com.olivet.entity.Location;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "locations", path = "location")
public interface LocationRepository extends PagingAndSortingRepository<Location, Long> {
}

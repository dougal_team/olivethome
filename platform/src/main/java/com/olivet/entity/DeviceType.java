package com.olivet.entity;

public enum DeviceType {
    AIR_TEMPERATURE,
    AIR_HUMIDITY,
    SOIL_HUMIDITY,
    WATER_TEMPERATURE
}

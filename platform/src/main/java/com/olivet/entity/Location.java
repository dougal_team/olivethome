package com.olivet.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private String name;
    private boolean enableAutoWatering;
    private boolean enableAutoFill;
    private int minHumidity;
    private int wateringIntervalInSeconds;
    private LocationType type;

    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL)
    private List<DeviceState> deviceStates = new ArrayList<>();


    @ManyToOne
    private OlivetUser olivetUser;

    public Location(){
    }

    public Location(String name ){
        this.name = name;
    }


    public OlivetUser getOlivetUser() {
        return olivetUser;
    }

    public void setOlivetUser(OlivetUser olivetUser) {
        this.olivetUser = olivetUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public List<DeviceState> getDeviceStates() {
        return deviceStates;
    }

    public void setDeviceStates(List<DeviceState> deviceStates) {
        this.deviceStates = deviceStates;
    }

    public boolean isEnableAutoWatering() {
        return enableAutoWatering;
    }

    public void setEnableAutoWatering(boolean enableAutoWatering) {
        this.enableAutoWatering = enableAutoWatering;
    }

    public boolean isEnableAutoFill() {
        return enableAutoFill;
    }

    public void setEnableAutoFill(boolean enableAutoFill) {
        this.enableAutoFill = enableAutoFill;
    }

    public int getMinHumidity() {
        return minHumidity;
    }

    public void setMinHumidity(int minHumidity) {
        this.minHumidity = minHumidity;
    }

    public int getWateringIntervalInSeconds() {
        return wateringIntervalInSeconds;
    }

    public void setWateringIntervalInSeconds(int wateringIntervalInSeconds) {
        this.wateringIntervalInSeconds = wateringIntervalInSeconds;
    }

    public LocationType getType() {
        return type;
    }

    public void setType(LocationType type) {
        this.type = type;
    }
}

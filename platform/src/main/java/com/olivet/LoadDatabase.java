package com.olivet;

import com.olivet.entity.*;
import com.olivet.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.*;

@Configuration
@Slf4j
public class LoadDatabase {

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository, LocationRepository locationRepository, DeviceStateRepository deviceStateRepository){
        return args -> {

            Role roles = new Role();
            roles.setRole("ROLE_USER");
            Role admin = new Role();
            admin.setRole("ROLE_ADMIN");
            roleRepository.save(roles);
            roleRepository.save(admin);

            OlivetUser olivetUser = new OlivetUser("admin@admin.com", "Oleksii");

            Location location = new Location("Home");
            olivetUser.setPassword(encoder.encode("password"));
            olivetUser.setRoles(new HashSet<>(Arrays.asList(roles)));
            olivetUser.setActive(1);
            location.setOlivetUser(olivetUser);

            List<Location> locations = new ArrayList<>();
            locations.add(location);

            olivetUser.setLocations(locations);

            Notification notification = new Notification("Новый пользователь зарегестрирован");
            Notification notification2 = new Notification("Клиент подключился");
            Notification notification3 = new Notification("Автоматический набор воды запущен");

            notification.setUser(olivetUser);
            notification2.setUser(olivetUser);
            notification3.setUser(olivetUser);

            olivetUser.setNotifications(Arrays.asList(notification, notification2, notification3));



            List<DeviceState> states = new ArrayList<>();

            Date currentDate = new Date();

            Calendar c = Calendar.getInstance() ;
            c.add(Calendar.DAY_OF_MONTH,-2);
            Date oldDate = c.getTime();

            Calendar c1 = Calendar.getInstance() ;
            c1.add(Calendar.HOUR,-1);
            Date oldDate1 = c1.getTime();

            Calendar c2 = Calendar.getInstance() ;
            c2.add(Calendar.HOUR,-2);
            Date oldDate2 = c2.getTime();

            Calendar c3 = Calendar.getInstance() ;
            c3.add(Calendar.HOUR,-3);
            Date oldDate3 = c3.getTime();

            Calendar c4 = Calendar.getInstance() ;
            c4.add(Calendar.HOUR,-4);
            Date oldDate4 = c.getTime();


            states.add(new DeviceState(DeviceType.SOIL_HUMIDITY, 10, new Date()));
            states.add(new DeviceState(DeviceType.SOIL_HUMIDITY, 20, oldDate4));
            states.add(new DeviceState(DeviceType.SOIL_HUMIDITY, 25, oldDate3));
            states.add(new DeviceState(DeviceType.SOIL_HUMIDITY, 22, oldDate2));
            states.add(new DeviceState(DeviceType.SOIL_HUMIDITY, 40, oldDate1));


            states.add(new DeviceState(DeviceType.SOIL_HUMIDITY, 25, oldDate));

            states.add(new DeviceState(DeviceType.AIR_HUMIDITY, 20, oldDate));
            states.add(new DeviceState(DeviceType.AIR_HUMIDITY, 30, new Date()));

            states.add(new DeviceState(DeviceType.AIR_TEMPERATURE, 13, oldDate));
            states.add(new DeviceState(DeviceType.AIR_TEMPERATURE, 15, new Date()));
            states.add(new DeviceState(DeviceType.WATER_TEMPERATURE, 3, new Date()));

            location.setDeviceStates(states);

            userRepository.save(olivetUser);
            locationRepository.save(location);

            notificationRepository.save(notification);
            notificationRepository.save(notification2);
            notificationRepository.save(notification3);



            for (DeviceState st: states) {
                st.setLocation(location);
                deviceStateRepository.save(st);
            }
        };
    }
}

package com.olivet.dto;

public class UserDto {
    private long id;
    private String name;
    private boolean nonExpired;
    private long locationId;
    private int refreshTime;
    private boolean autoWateringEnabled;
    private boolean autoFillWater;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNonExpired() {
        return nonExpired;
    }

    public void setNonExpired(boolean nonExpired) {
        this.nonExpired = nonExpired;
    }

    public UserDto(long id, String name, boolean nonExpired, long locationId){
        this.id = id;
        this.name = name;
        this.nonExpired = nonExpired;
        this.locationId = locationId;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public int getRefreshTime() {
        return refreshTime;
    }

    public void setRefreshTime(int refreshTime) {
        this.refreshTime = refreshTime;
    }

    public boolean isAutoWateringEnabled() {
        return autoWateringEnabled;
    }

    public void setAutoWateringEnabled(boolean autoWateringEnabled) {
        this.autoWateringEnabled = autoWateringEnabled;
    }

    public boolean isAutoFillWater() {
        return autoFillWater;
    }

    public void setAutoFillWater(boolean autoFillWater) {
        this.autoFillWater = autoFillWater;
    }
}
